<?php

namespace App\Helpers\Arctic;

use \Arctic\Model\Trip\Trip as ArcticTrip;
use \Arctic\Model\Inquiry\Inquiry as ArcticInquiry;
use \Arctic\Model\Person\Person as ArcticPerson;
use \Arctic\Model\Reservation\Reservation as ArcticReservation;
use \Arctic\Model\Reservation\ReservationMember as ArcticReservationMember;
use \Arctic\Model\Activity\Activity as ArcticActivity;
use \Arctic\Model\Invoice\Invoice as ArcticInvoice;
use \Arctic\Model\TripForm\Response as ArcticForm;

use DateInterval;
use DateTime;

// Arctic API Initiation
require_once( 'arctic/includes/Api.php' );

// initiate Arctic API
// PARAMETERS:
// installation_name: the subdomain of your Arctic installation (e.g., "float" if your installation is float.arcticres.com)
// API username and password: used to authenticate against the installation and determine permission levels
// Client ID and secret: the credentials used to authenticate your app, does not affect permission levels
define('ARCTIC_INSTALLATION_NAME', 'aorafting');
define('ARCTIC_API_USER', 'hjsljhomtrie');
define('ARCTIC_API_PASS', 'Zx4$tvj9B@#a@CdzZCCifM4c');
define('ARCTIC_CLIENT_ID', 'lccromknczol');
define('ARCTIC_SECRET', 'UbOdNWRLKUh7b-3B!6supLv6');

\Arctic\Api::init(ARCTIC_INSTALLATION_NAME, ARCTIC_API_USER, ARCTIC_API_PASS, [
  'client_id'     => ARCTIC_CLIENT_ID,
  'client_secret' => ARCTIC_SECRET
]);


class StringHelper
{
  /**
   * Is the string non-empty?
   *
   * @param {string} $str
   * @return {bool}
   */
  public static function hasValue($str) {
    return isset($str) && $str != '' && !ctype_space($str);
  }

  /**
   * Hour stamp to days
   *
   * @param {string} $str
   * @return {int}
   */
  public static function hourStampToDays($str) {
    return explode(':', $str)[0] / 24;
  }
}


class Arctic
{
  

  public static function recentInquiryPeople($days = 2) {
    $start_date = date('y-m-d g:i:s', strtotime('-' . $days . ' days'));
    $inquiries = ArcticInquiry::query("ANY person.emailaddresses WHERE (subscribetoemaillist = true) AND modifiedon >= '" . $start_date . "' ORDER BY modifiedon DESC");
    $return_people = array();

    foreach( $inquiries as $inquiry ): 
      $this_inquiry = $inquiry->toArray();
      foreach ( $inquiry->person->emailaddresses  as $ea ) {
        $return_people[] = array(
          'id' => $this_inquiry['id'],
          'created_date' => $this_inquiry['createdon'],
          'modified_date' => $this_inquiry['modifiedon'],
          'namefirst' => $inquiry->person->namefirst,
          'namelast' => $inquiry->person->namelast,
          'tripid' => $inquiry->trip->id,
          'tripname' => $inquiry->trip->name,
          'email' => $ea->emailaddress,
          'subscribe' => $ea->subscribetoemaillist,
          'isprimary' =>$ea->isprimary,
          'type' => 'inquiry'
        );
      }
    endforeach;

    return $return_people;
  }


  public static function recentReservationsPeople($days = 2) {
    $start_date = date('y-m-d g:i:s', strtotime('-' . $days . ' days'));
    $yesterday = date('Y-m-d', strtotime('-1 day'));
    $activities = ArcticActivity::query("ANY person.emailaddresses WHERE (subscribetoemaillist = true) AND modifiedon >= '" . $start_date . "' AND start <= '" . $yesterday . "' ORDER BY modifiedon DESC");
    $return_people = array();

    foreach ($activities as $activity) {
      $this_act = $activity->toArray();
      $activity_id = $this_act['id'];
      $is_trip_leader = 'yes';

      // Only Include Primary Person
      if($this_act['parentactivityid'] != 0) {
        // continue;
        $activity_id = $this_act['parentactivityid'];
        $is_trip_leader = 'no';
      }

      $activity_start = $this_act['start'];
      $activity_start_mod = str_replace('00:00:00', '12:01:00', $activity_start);
      $reservation = ArcticReservation::load($activity_id);

      // Calculate 
      $new_res = array(
        'tripid' => $reservation->trip->id,
        'tripname' => $reservation->trip->name,
        'activityid' => $this_act['id'],
        'personid' => $this_act['personid'],
        'activity_start' => $activity_start_mod,
        'activity_createdon' => $this_act['createdon'],
        'activity_status' => $this_act['status'],
        'namefirst' => $activity->person->namefirst,
        'namelast' => $activity->person->namelast,
        'type' => 'reservation',
        'tripleader' => $is_trip_leader,
      );

      // only process if person has an email configured.
      if($activity->person->emailaddresses):
        foreach ( $activity->person->emailaddresses  as $ea ) {
          if($ea->isprimary){
            $new_res['email'] = $ea->emailaddress;
            $new_res['subscribe'] = $ea->subscribetoemaillist;
            $new_res['isprimary'] = $ea->isprimary;
          }
        }

        $return_people[] = $new_res;
      endif;
    }

    return $return_people;
  }


  public static function recentActivitySales() {
    $start_date = date('y-m-d g:i:s', strtotime('-5 days'));
    $end_date = date('y-m-d g:i:s', strtotime('-3 days'));
    
    // $activities = ArcticActivity::query("ANY person.emailaddresses WHERE (subscribetoemaillist = true) AND modifiedon >= '" . $start_date . "' ORDER BY modifiedon DESC");
    $activities = ArcticActivity::query("start >= '" . $start_date ."' AND start < '" . $end_date ."' AND invoice.status = 'paid'");

    $return_people = array();

    foreach ($activities as $activity) {
      $this_act = $activity->toArray();
      $activity_id = $this_act['id'];
      $is_trip_leader = 'yes';

      // Check if Primary Person
      if($this_act['parentactivityid'] != 0) {
        $activity_id = $this_act['parentactivityid'];
        $is_trip_leader = 'no';
      }

      $activity_start = $this_act['start'];
      $activity_start_mod = str_replace('00:00:00', '12:01:00', $activity_start);

      // Skip Cancelled
      if($this_act['status'] == 'canceled'){
        continue;
      }

      $invoice = $activity->invoice;

      // Calculate 
      $new_res = array(
        'activityid' => $this_act['id'],
        'personid' => $this_act['personid'],
        'activity_start' => $activity_start_mod,
        'activity_createdon' => $this_act['createdon'],
        'activity_status' => $this_act['status'],
        'namefirst' => $activity->person->namefirst,
        'namelast' => $activity->person->namelast,
        'type' => 'reservation',
        'tripleader' => $is_trip_leader,
        'invoice_id' => $activity->invoice->id,
        'invoice_total' => $activity->invoice->totalcost,
        'invoice_balance' => $activity->invoice->balancedue,
      );

      // only process if person has an email configured.
      if($activity->person->emailaddresses):
        foreach ( $activity->person->emailaddresses  as $ea ) {
          if($ea->isprimary){
            $new_res['email'] = $ea->emailaddress;
            $new_res['subscribe'] = $ea->subscribetoemaillist;
            $new_res['isprimary'] = $ea->isprimary;
          }
        }

        $return_people[] = $new_res;
      endif;
    }

    return $return_people;
  }




  public static function recentTripSales() {
    $start_date = date('y-m-d g:i:s', strtotime('-3 days'));
    $end_date = date('y-m-d g:i:s', strtotime('today'));
    
    // $activities = ArcticActivity::query("ANY person.emailaddresses WHERE (subscribetoemaillist = true) AND modifiedon >= '" . $start_date . "' ORDER BY modifiedon DESC");
    $activities = ArcticActivity::query("start >= '" . $start_date ."' AND start < '" . $end_date ."' AND invoice.status = 'paid'");



    // TESTING
    ///
    ////
    /////
    //////
    ///////

    $return_people = array();

    foreach ($activities as $activity) {
      $this_act = $activity->toArray();
      $activity_id = $this_act['id'];
      $is_trip_leader = 'yes';

      // Check if Primary Person
      if($this_act['parentactivityid'] != 0) {
        $activity_id = $this_act['parentactivityid'];
        $is_trip_leader = 'no';
      }

      $activity_start = $this_act['start'];
      $activity_start_mod = str_replace('00:00:00', '12:01:00', $activity_start);
      $reservation = ArcticReservation::load($activity_id);

      // Skip Cancelled
      if($reservation->trip->canceled === true || $this_act['status'] == 'canceled'){
        continue;
      }

      $invoice = $activity->invoice;

      // Calculate 
      $new_res = array(
        'tripid' => $reservation->trip->id,
        'tripname' => $reservation->trip->name,
        'activityid' => $this_act['id'],
        'personid' => $this_act['personid'],
        'activity_start' => $activity_start_mod,
        'activity_createdon' => $this_act['createdon'],
        'activity_status' => $this_act['status'],
        'namefirst' => $activity->person->namefirst,
        'namelast' => $activity->person->namelast,
        'type' => 'reservation',
        'tripleader' => $is_trip_leader,
        'invoice_id' => $activity->invoice->id,
        'invoice_total' => $activity->invoice->totalcost,
        'invoice_balance' => $activity->invoice->balancedue,
      );

      // only process if person has an email configured.
      if($activity->person->emailaddresses):
        foreach ( $activity->person->emailaddresses  as $ea ) {
          if($ea->isprimary){
            $new_res['email'] = $ea->emailaddress;
            $new_res['subscribe'] = $ea->subscribetoemaillist;
            $new_res['isprimary'] = $ea->isprimary;
          }
        }

        $return_people[] = $new_res;
      endif;
    }

    return $return_people;
  }



  public static function recentInvoicePayments() {
    $start_date = date('y-m-d g:i:s', strtotime('-3 days'));
    $end_date = date('y-m-d g:i:s', strtotime('today'));
    
    // $activities = ArcticActivity::query("ANY person.emailaddresses WHERE (subscribetoemaillist = true) AND modifiedon >= '" . $start_date . "' ORDER BY modifiedon DESC");
    $activities = ArcticInvoice::query("modifiedon >= '" . $start_date ."' AND ANY transactions WHERE (type = 'payment' AND time >= '" . $start_date . "')");

    $return = array();

    foreach ($activities as &$activity) {
      $this_act = $activity->toArray();
      $transactions = $activity->transactions;
      $trans = array();

      foreach ($activity->transactions as $key => $tr) {
        $trans[] = array(
          'invoiceid' => $tr->invoiceid,
          'id' => $tr->id,
          'type' => $tr->type,
          'description' => $tr->description,
          'amount' => $tr->amount,
          'referenceclass' => $tr->referenceclass,
          'referenceid' => $tr->referenceid,
          'time' => $tr->time,
          'isbatched' => $tr->isbatched,
          'settled' => $tr->settled,
          'batchid' => $tr->batchid,
          'createdon' => $tr->createdon,
          'modifiedon' => $tr->modifiedon,
          'createdbyuserid' => $tr->createdbyuserid
        );
      }

      $return[] = array(
        'activity' => $this_act,
        'transactions' => $trans
      );

    }
    return $return;
  }


  protected static function get_person($person_object){
    $person = array(
      'namefirst' => $person_object->namefirst,
      'namelast' => $person_object->namelast,
    );


    if($person_object->emailaddresses){
      foreach ( $person_object->emailaddresses  as $ea ) {
        if($ea->isprimary){
          $person['email'] = $ea->emailaddress;
          $person['subscribe'] = $ea->subscribetoemaillist;
          $person['isprimary'] = $ea->isprimary;
        }
      }
    }

    return (object) $person;
  }

  public static function get_recent_payments($start = '-7 days', $end = 'today') {
    $start_date = date('y-m-d g:i:s', strtotime($start));
    $end_date = date('y-m-d g:i:s', strtotime($end));
    
    // $activities = ArcticActivity::query("ANY person.emailaddresses WHERE (subscribetoemaillist = true) AND modifiedon >= '" . $start_date . "' ORDER BY modifiedon DESC");
    $activities = ArcticInvoice::query("modifiedon >= '" . $start_date ."' AND ANY transactions WHERE (type = 'payment' AND time >= '" . $start_date . "')");

    $return = array();

    foreach ($activities as &$activity) {
      $payment = array();
      $trans = array();

      $this_act = $activity->toArray();
      $transactions = $activity->transactions;
      $this_person = self::get_person($activity->person);
      $act_modified = $activity->modifiedon; // date time string

      $payment = array(
        'activityid' => $this_act['id'],
        'personid' => $this_act['personid'],
        'activity_modifiedon' => $this_act['modifiedon'],
        'activity_createdon' => $this_act['createdon'],
        'namefirst' => $this_person->namefirst,
        'namelast' => $this_person->namelast,
        'email' => $this_person->email,
        'subscribe' => $this_person->subscribe,
        'isprimary' => $this_person->isprimary,
      );



      foreach ($activity->transactions as $key => $tr) {

        //
        // we want only one transaction
        // 
        // so if this activity only has one, we take that.
        // OR if this activity has MORE than one, 
        //   we check the datetime the transaction was created and compare to the parent activity modified, 
        //   if those are the same then we have the transaciton we want. 
        //
        $trans_created = $tr->createdon; // date time string
        $dt_activity = new DateTime($this_act['modifiedon']);
        $dt_trans = new DateTime($tr->createdon);

        if(count($transactions) || $dt_trans >= $dt_activity){
          $trans = array(
            'invoiceid' => $tr->invoiceid,
            'id' => $tr->id,
            'type' => $tr->type,
            'description' => $tr->description,
            'amount' => $tr->amount,
            'referenceclass' => $tr->referenceclass,
            'referenceid' => $tr->referenceid,
            'time' => $tr->time,
            'isbatched' => $tr->isbatched,
            'settled' => $tr->settled,
            'batchid' => $tr->batchid,
            'createdon' => $tr->createdon,
            'modifiedon' => $tr->modifiedon,
            'createdbyuserid' => $tr->createdbyuserid
          );
          break;
        }
      }

      $return[] = (object) array(
        'invoice' => (object) $this_act,
        'transaction' => (object) $trans,
        'person' => $this_person
      );



    }
    return $return;


  }
}
