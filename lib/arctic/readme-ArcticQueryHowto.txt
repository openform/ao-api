Thanks for your patience. I was finally able to dig in last night and find what we needed.

The arguments that can be used in your requests to specify number of requests, cycle through results and pages are:

number

start

page



Number and start are the ones, because both deal with entries for their arguments and your response tells you how many entries you are working with so it keeps things straight forward.

However, you can also use page to get cycle the entries. You will have to do some math to figure out how many pages are needed to cycle through based on the number of results you are having returned but that is possible.

The max number of results you can return on any call is 200. If you enter an argument greater than 200 the system will revert to the default of 50.

Here are some sample queries showing the use of the number, start and page arguments on a query for all inquiries created in 2020.



Number Only (&number=200) — Provides the first 200 entries

https://riverrunners.arcticres.com/api/rest/inquiry?query=createdon => '2020-01-01' AND createdon <= '2021-12-31'&number=200



Number & Start Combined (&number=200&start=200) — Yields the next 200 entries.

https://riverrunners.arcticres.com/api/rest/inquiry?query=createdon => '2020-01-01' AND createdon <= '2021-12-31'&number=200&start=200



Number & Page (&number=200&page=1) — Yields the first 200 entries

https://riverrunners.arcticres.com/api/rest/inquiry?query=createdon => '2020-01-01' AND createdon <= '2021-12-31'&number=200&page=1

Increasing the page number to 2 will yield the next 200 entries and so on.



Let us know if you have any additional questions and we will be happy to help.

Best,

~m

Tech Support

Arctic Reservations

(601) ARCTIC 9 or (601) 272 - 8429
support@arcticreservations.com