<?php 

namespace App\Helpers\Mailchimp;

// Mailchimp PHP Library
require_once('mailchimp/vendor/autoload.php');

class Mailchimp
{ 

  /**
   * Initiate Mailchimp Connection
   *
   * @return Object = Mailchimp for use in all other calls
   */
  protected static function connect(){
    // Initiate Mailchimp Connection
    $mailchimp = new \MailchimpMarketing\ApiClient();
    $mailchimp->setConfig([
      'apiKey' => '968a03d3666834c9bf92950b2664790f-us8',
      'server' => 'us8'
    ]);
    return $mailchimp;
  }


  /**
   * Test the Mailchimp Connection
   *
   * @return String
   */
  public static function test() {
    $mailchimp = Mailchimp::connect();
    $response = $mailchimp->ping->get();
     return $response;
  }

  /**
   * Get all existing lists for this account
   * 
   * @return lists
   * 
   */
  protected static function get_lists() {
    $mailchimp = Mailchimp::connect();
    $response = $mailchimp->lists->getAllLists();
    $result = array();
    foreach($response->lists as $list){
      $result[] = array(
        'id' => $list->id,
        'name' => $list->name,
        'count' => $list->stats->member_count
      );
    }
    return $result;
  }


  /**
   * Get memebrs from a sepcific list
   * Returns qty of 10 only
   * 
   * 
   * @param $list_id = ID of Mailchimp list
   * @return members
   * 
   */
  public static function get_members($list_id){
    $mailchimp = Mailchimp::connect();
    $response = $mailchimp->lists->getListMembersInfo($list_id);
    return $response;
  }


/**
   * Get memeber activity feed
   * 
   * 
   * @param $list_id = ID of Mailchimp list
   * @param $email = Member Email Address
   * @return array I think.
   * 
   */
  public static function get_member_activityfeed($list_id, $email, $fields = null, $exclude_fields = null, $count = '10', $offset = '0', $activity_filters = null){
    $mailchimp = Mailchimp::connect();
    $response = $mailchimp->lists->getListMemberActivityFeed($list_id, md5($email), $fields, $exclude_fields, $count, $offset, $activity_filters);
    return $response;
  }



  /**
   * Search memebrs 
   * Returns json
   * 
   * 
   * Example Return: https://mailchimp.com/developer/marketing/api/search-members/search-members/
   * 
   * @param $list_id = ID of Mailchimp list
   * @returns OBJECT with keys: id, email_address
   * 
   */
  public static function search_members($list_id, $email){
    $mailchimp = Mailchimp::connect();

    // search($query, $fields = null, $exclude_fields = null, $list_id = null)
    $fields = ['exact_matches.members.id', 'exact_matches.members.email_address'];
    $response = $mailchimp->searchMembers->search($email, $fields, null, $list_id);
    $array_of_matches = $response->exact_matches->members;
    if(count($array_of_matches) !== 1){
      return false;
    }
    return $array_of_matches[0];
  }


  /**
   * Add multiple email subscribers at onces
   * 
   * Will NOT update member info if it exsits
   * 
   * @param $users_array = array($email, $fname, $lname);
   * @return Object including new members
   * 
   */
  public static function add_user($email, $name){
    $mailchimp = Mailchimp::connect();

    // Main Mailchimp List
    $list_id = 'a02be43772'; // AO Communications

    // Email parameter for subscriber_hash
    $email_lower = strtolower($email);

    $response = $mailchimp->lists->setListMember($list_id, $email_lower, [
        "email_address" => $email,
        "full_name" => $name,
        "merge_fields" => array(
          "FNAME" => 'no interest',
          "LNAME" => 'except the real',
        ),
        "status_if_new" => "subscribed",
    ], true);
    return $response;
  }



  /**
   * Add multiple email subscribers at onces
   * 
   * Will NOT update member info if it exsits
   * 
   * @param $users_array = array with params 'email', 'fname', 'lname'
   * @example $users_array = array(['email'=> 'one@openform.co','fname'=>'billy','lname'=>'bobby'], ...)
   * @return Object including new members
   * 
   */
  public static function add_users_bulk($users_array){
    $mailchimp = Mailchimp::connect();

    // Main Mailchimp List
    $list_id = 'a02be43772'; // AO Communications

    // Setup Batch
    $batch = array();

    foreach($users_array as $u){
      $object = (object) array(
        "email_address" => $u['email'],
        "status" => "subscribed",
        "merge_fields" => array(
          "FNAME" => $u['fname'],
          "LNAME" => $u['lname'],
        ),
      );
      array_push($batch, $object);
    }

    $response = $mailchimp->lists->batchListMembers($list_id, ["members" => $batch, "sync_tags" => false, "update_existing" => false ]);
    return $response;
  }


  /**
   * Add multiple email subscribers at onces
   * 
   * Will NOT update member info if it exsits
   * 
   * @param $users_array = array with params 'email', 'fname', 'lname'
   * @example $users_array = array(['email'=> 'one@openform.co','fname'=>'billy','lname'=>'bobby'], ...)
   * @return Object including new members
   * 
   */
  public static function add_users_bulk_batchready($users_object){
    $mailchimp = Mailchimp::connect();

    // Main Mailchimp List
    $list_id = 'a02be43772'; // AO Communications

    $response = $mailchimp->lists->batchListMembers($list_id, ["members" => $users_object, "sync_tags" => false, "update_existing" => false ]);
    return $response;
  }



  /**
   * GET all Mailchimp Ecommerce Stores
   * 
   */

  public static function get_stores() {
    $mailchimp = Mailchimp::connect();
    $response = $mailchimp->ecommerce->stores();
    return $response;
  }


  /**
   * ADD a Mailchimp Ecommerce Store
   * 
   * ONLY RUN THIS ONCE!
   */

  public static function add_store() {
    $mailchimp = Mailchimp::connect();

    $response = $mailchimp->ecommerce->addStore([
        "id" => "arctic-ao",
        "list_id" => "a02be43772",
        "name" => "AO Arctic Reservations",
        "currency_code" => "USD",
        "primary_locale" => "en_US",
        "timezone" => "America/Los_Angeles",
    ]);

    return $response;
  }








  /**
   * Get all orders
   * 
   */
  public static function get_orders() {
    $mailchimp = Mailchimp::connect();
    $store_id = 'arctic-ao';
    $response = $mailchimp->ecommerce->getStoreOrders($store_id);
    return $response;
  }



  /**
   * Get all products
   * 
   */
  public static function get_products() {
    $mailchimp = Mailchimp::connect();
    $store_id = 'arctic-ao';
    $response = $mailchimp->ecommerce->getAllStoreProducts($store_id);
    return $response;
  }


  /**
   * ADD a product
   * 
   */
  public static function add_product_test() {
    $mailchimp = Mailchimp::connect();
    $store_id = 'arctic-ao';


    // Config
    $product_id = "GC";
    $product_title = "Gift Certificate";
    $variants = [["id" => "gc1", "title" => "arctic gift certificate"]];


    $response = $mailchimp->ecommerce->addStoreProduct($store_id, [
        "id" => $product_id,
        "title" => $product_title,
        "variants" => $variants,
    ]);
    return $response;
  }






  /**
   * Add single ORDER to a CUSTOMER in Mailchimp
   * 
   * Ref:  https://mailchimp.com/developer/marketing/api/ecommerce-orders/add-order/
   * 
   * 
   * $order_object REQUIRED fields
   * ---------------------
   * @param  string  $type             Arctic type of order (must equal "reservation" to process)
   * @param  string  $mcid             Mailchimp customer id (must exist in MC)
   * @param  int     $activityid       Arctic activity id (unique)
   * @param  int     $transaction_id   Arctic transaction id (unique)
   * @param  int     $personid         Arctic person id (unique)
   * @param  string  $email     Arctic person email 
   * @param  string  $email            Arctic primary email address 
   * @param  float   $amount    Arctic invoice amount (should equal amount of this payment on inv)
   * @param  string  $date             Arctic date and time of transaction
   * 
   */
  public static function add_order($order_object) {


    $mailchimp = Mailchimp::connect();


    // ONLY invoice payments for now. 
    // FUTURE TODO: add this for certificate purchases.   
    if($order_object->type != 'payment') return false;


    try {

      $store_id = 'arctic-ao';
      $customer_id = $order_object->mcid;
      // $trip_id = $order_object->tripid ?? NULL;
      $activity_id = $order_object->activityid ?? NULL;
      $person_id = $order_object->personid ?? NULL;
      $email = $order_object->email ?? NULL;
      $amount = $order_object->amount ?? NULL;
      $mcid = $order_object->mcid ?? NULL;
      $date = $order_object->date;

      
      // check if anything missing
      if(!$activity_id || !$person_id || !$email || !$amount || !$mcid) return false;

      // Build Unique order id for specific person and activity
      // Purpose: to avoid double entry of invoices to MC.
      $order_id = $activity_id . '-' . $person_id;

      // Product Vars for RESERVATIONS
      $product_id = 'TRIP';
      $variant_id = 'rt1';

      $special_date_for_mailchimp = date("c", strtotime($date));

      $response = $mailchimp->ecommerce->addStoreOrder($store_id, [
          "id" => $order_id,
          "customer" => [
              "id" => $mcid,
              "opt_in_status" => true,
              "email_address" => $email
          ],
          "processed_at_foreign" => $special_date_for_mailchimp,
          "currency_code" => "USD",
          "order_total" => $amount,
          "lines" => [
              [
                  "id" => '1',
                  "product_id" => $product_id,
                  "product_variant_id" => $variant_id,
                  "quantity" => 1,
                  "price" => $amount,
              ],
          ],
      ]);


    } catch (\EXCEPTION $e) {

      return $e->getResponse()->getBody()->getContents();

    }


    return $response;
  }



  /**
   * Add single ORDER to a CUSTOMER in Mailchimp
   * 
   * Accepts: Array of Arrays (individual activities)
   * 
   * Array
        (
            [tripid] => 8490
            [tripname] => Middle Fork American 1-Day
            [activityid] => 16368
            [personid] => 273661
            [activity_start] => 2022-08-07 08:00:00
            [activity_createdon] => 2022-07-07 15:10:54
            [activity_status] => over
            [namefirst] => Jorge
            [namelast] => Mora
            [type] => reservation
            [tripleader] => yes
            [invoice_id] => 15190
            [invoice_total] => 1592.35
            [invoice_balance] => 0.00
            [email] => 4extremeautobody@gmail.com
            [subscribe] => 
            [isprimary] => 1
        )
   * 
   */
  public static function bulk_add_orders($arctic_activities) {

    // Build function response
    $responses = array();

    // Mailchimp Connect 
    $mailchimp = Mailchimp::connect();

    // Loop Activities from Arctic
    foreach ($arctic_activities as $key => $activity) {
      
      // ONLY reservations for now. 
      // FUTURE TODO: add this for certificate purchases.   
      if($activity['type'] != 'reservation') continue;


      try {
        $store_id = 'arctic-ao'; // Mailchim ECom Store (already setup)
        $trip_id = $activity['tripid'] ?? NULL;
        $activity_id = $activity['activityid'] ?? NULL; 
        $person_id = $activity['personid'] ?? NULL; 
        $person_email = $activity['email'] ?? NULL;
        $invoice_total = $activity['invoice_total'] ?? NULL;
        $subscribe = $activity['subscribe'] ?? NULL;

        // check if anything missing
        if(!$trip_id || !$activity_id || !$person_id || !$person_email || !$invoice_total) continue;

        // Check if subscribe is checked in arctic
        if(!$subscribe) continue;


        // Build Unique order id for specific person and activity
        // Purpose: to avoid double entry of invoices to MC.
        $order_id = $trip_id . '-' .$activity_id . '-' . $person_id;


        // Product Vars for RESERVATIONS
        $product_id = 'TRIP';
        $variant_id = 'rt1';

        // Send to MC - individually
        $response = $mailchimp->ecommerce->addStoreOrder($store_id, [
            "id" => $order_id,
            "customer" => [
                "id" => '1',
                "opt_in_status" => true,
                "email_address" => $person_email
            ],
            "currency_code" => "USD",
            "order_total" => $invoice_total,
            "lines" => [
                [
                    "id" => '1',
                    "product_id" => $product_id,
                    "product_variant_id" => $variant_id,
                    "quantity" => 1,
                    "price" => $invoice_total,
                ],
            ],
        ]);


      } catch (\EXCEPTION $e) {

        return $e->getResponse()->getBody()->getContents();

      }

      $responses[] = $response;
    }


    return $responses;
  }

}

