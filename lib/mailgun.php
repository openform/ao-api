<?php 

namespace App\Helpers\Mailgun;

// Mailgun API Library
require_once('mailgun/vendor/autoload.php');
use Mailgun\Mailgun;

class Email
{   
    protected static $key = "key-9110c832f9e927cafae6fdec30fb08a3";

    public static function send_email($to, $subject, $body){


        $mg = Mailgun::create("key-9110c832f9e927cafae6fdec30fb08a3");
        $domain = "lab.openform.io";

        $result = $mg->messages()->send($domain, [
            'from' => 'alloutdoor@lab.openform.io',
            'to' => $to,
            'subject' => $subject,
            'text' => $body
        ]);
        return $result;
    }
}
