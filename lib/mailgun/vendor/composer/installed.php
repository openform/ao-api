<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '094afaca7b6d815db82d2059c07e40169db8c70e',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '094afaca7b6d815db82d2059c07e40169db8c70e',
            'dev_requirement' => false,
        ),
        'guzzle/batch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/common' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/guzzle' => array(
            'pretty_version' => 'v3.9.3',
            'version' => '3.9.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzle/guzzle',
            'aliases' => array(),
            'reference' => '0645b70d953bc1c067bbc8d5bc53194706b628d9',
            'dev_requirement' => false,
        ),
        'guzzle/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/inflection' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/iterator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/parser' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-async' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-backoff' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-curlauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-error-response' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-history' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-md5' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-mock' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-oauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/service' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/stream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'mailgun/mailgun-php' => array(
            'pretty_version' => 'v1.7.2',
            'version' => '1.7.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mailgun/mailgun-php',
            'aliases' => array(),
            'reference' => '45ec0c8f3a2a6554b4987e97889f44991e4f75b4',
            'dev_requirement' => false,
        ),
        'nyholm/psr7' => array(
            'pretty_version' => '1.5.0',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7',
            'aliases' => array(),
            'reference' => '1461e07a0f2a975a52082ca3b769ca912b816226',
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/message-factory' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message-factory',
            'aliases' => array(),
            'reference' => 'a478cb11f66a6ac48d8954216cfed9aa06a501a1',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'fe5ea303b0887d5caefd3d431c3e61ad47037001',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => 'a77e974a5fecb4398833b0709210e3d5e334ffb0',
            'dev_requirement' => false,
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v6.0.5',
            'version' => '6.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'reference' => 'a8f87328930932c455cffd048f965d1223d91915',
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'reference' => '265f03fed057044a8e4dc159aa33596d0f48ed3f',
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '3.0',
            ),
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => '36715ebf9fb9db73db0cb24263c79077c6fe8603',
            'dev_requirement' => false,
        ),
    ),
);
