<?php 
/**
 * get recent payments from Arctic
 * send to Mailchimp
 */

namespace App\AO;  

require_once('lib/arctic.php');
require_once('lib/mailchimp.php');
use \App\Helpers\Arctic\Arctic as ArcticHelper;
use App\Helpers\Mailchimp\Mailchimp as MC;


/*
1. get all recent transactions (invoice payments)
    index on the email address.

2. build orders for import
    create order structure for MC from the Arctic info.

3. Batch and send to Mailcimp


to run cron

use App\AO\ArcticMC as ArcticMC;
ArcticMC::run();

*/


class ArcticMC
{ 
  /**    
    * Prints a message to the debug file that can easily be called by any subclass.     
    *     
    * @param mixed $message      an object, array, string, number, or other data to write to the debug log    
    * @param bool  $shouldNotDie whether or not the The function should exit after writing to the log     
    *     
    */   
  protected static function log($message, $shouldNotDie = false) {
    error_log(print_r($message, true));   
    if ($shouldNotDie) {
      exit;
    }
  }


  protected static function logger($message) {
    try {
      $url = 'https://jumpsand.com/logger?key=79831acf-b88f-4917-8e9e-ccc1ee2d5451';
      $source = 'aorafting';
      $logtime = time();
      $data = array(
          'results' => $message,
          'timestamp' => $logtime,
          'source' => $source
      );

      // use key 'http' even if you send the request to https://...
      $options = array(
          'http' => array(
              'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
              'method'  => 'POST',
              'content' => http_build_query($data)
          )
      );
      $context  = stream_context_create($options);
      $result = file_get_contents($url, false, $context);
      if ($result === FALSE) { 
        self::log('Attempt to send log to Jumpsand failed');
      } else {
        self::log('Attempt to send log to Jumpsand was a SUCCESS! ');
        self::log('Result: ' . $result);
      }
      return 'logged to Jump';
    } catch (Exception $e) {
      self::log('Jump Log Caught. darn.');
      return 'ok, no Jump log though';
    }
  }
  
  public static function run() {

  
    $time1 = time();
    self::log('Initiating Arctic Sync Recent Payments to Mailchimp process run()');
        
    $recentPayments = ArcticHelper::get_recent_payments();
    $time2 = time();
    self::log('Got recent payments');


    //
    // #1
    // BUILD single array 
    // indexed by email
    $subscribers = array();
    $tolog = array();


    // GET Payment Transactions
    foreach ($recentPayments as $payment) {
      $this_email = NULL;
      $this_log = array();

      // Only take those checked as Subscribe to mailing in Arctic.
      if(!$payment->person->subscribe) continue;

      $this_email = $payment->person->email;

      $subscribers[$this_email] = (object) array(
        'activityid' => $payment->invoice->id,
        'transaction_id' => $payment->transaction->id,
        'date' => $payment->transaction->createdon,
        'personid' => $payment->invoice->personid,
        'namefirst' => $payment->person->namefirst,
        'namelast' => $payment->person->namelast,
        'email' => $payment->person->email,
        'type' => $payment->transaction->type,
        'amount' => $payment->transaction->amount,
        'subscribe' => $payment->person->subscribe,
        'mcid' => ''
      );

      self::log('Arctic Transaction: ' . $payment->transaction->id . '-' . $payment->invoice->personid . ' : total ' . $payment->transaction->amount  );
      $this_log['arctictransaction'] = $payment->transaction->id . '-' . $payment->invoice->personid . ' : total ' . $payment->transaction->amount;
       

      $list_id = 'a02be43772';
      $mc_subscriber = MC::search_members($list_id, $this_email);

      // check if we have this person in MC already, if so, grab the ID.
      if($mc_subscriber && isset($mc_subscriber->id)){
        $subscribers[$this_email]->mcid = $mc_subscriber->id;
        self::log('Subscriber found for: ' . $this_email . ' = id: ' . $mc_subscriber->id ); 
        $this_log['subscriber'] = 'Subscriber found for: ' . $this_email . ' = id: ' . $mc_subscriber->id;

      } else {

        // IF the person is NOT in mailchimp, add them.        
        $mc_subscriber = MC::add_user($this_email, $payment->person->namefirst . ' ' . $payment->person->namelast);
        $subscribers[$this_email]->mcid = $mc_subscriber->id;
        self::log('Subscriber ADDED for: ' . $this_email . ' = id: ' . $mc_subscriber->id );
        $this_log['subscriber'] = 'Subscriber ADDED for: ' . $this_email . ' = id: ' . $mc_subscriber->id;
      }

      array_push($tolog, $this_log);

    }
    self::log('Total to import: ' . count($subscribers));
    $time3 = time();
    self::log('Built Subscribers array');



    //
    // #2
    // Send Recent Payments to Mailchimp
    //
    $add_orders_responses = array();
    foreach($subscribers as &$me) {
      self::log('Sending payment details for ' . $me->email);
      $me->response = MC::add_order($me);
    }
    $time4 = time();
    self::log('Sent all orders to MC');



    $times = array(
      'one' => $time2 - $time1,
      'two' => $time3 - $time2,
      'three' => $time4 - $time3
    );

    // print the time it took for each step in SECONDS
    self::log('Timing: Arctic Sales: ' . $times['one'] .  
      ' — check subscribers in mailchimp: ' . $times['two'] . 
      ' — send orders to mailchimp: ' . $times['three']);


    echo '<pre>';
    print_r($subscribers);
    echo '</pre>';


    // Try to log data to Jumpsand Lab Cloud Data STore
    $processed = array(
      'count' => count($subscribers),
      'data' => $tolog
    );

    self::logger(json_encode($processed));


    return 'complete';
  }
}

?>