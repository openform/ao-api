<?php

// TESTING Arctic to Mailchimp service
require_once('lib/arctic.php');
require_once('lib/mailchimp.php');
require_once('neworders.php');
use \App\Helpers\Arctic\Arctic as ArcticHelper;
use App\Helpers\Mailchimp\Mailchimp as MC;
use \Arctic\Model\Invoice\Invoice as ArcticInvoice;


use App\AO\ArcticMC as ArcticMC;
// ArcticMC::run();



// function log($message, $shouldNotDie = false) {
//   error_log(print_r($message, true));   
//   if ($shouldNotDie) {
//     exit;
//   }
// }



// // Trip Invoices - TEST
// $response = ArcticHelper::recentInvoicePayments();
// $response = ArcticHelper::recentActivitySales();
// $response = ArcticHelper::get_recent_payments();
$response = ArcticMC::run();



// Mailchimp Members
// $list_id = 'a02be43772';
// $response = MC::get_members($list_id);
// $response = MC::search_members($list_id, 'austin@openform.co');
// $response = MC::add_user('new@openform.co', "test austin");


// Mailchimp Stores
// -------------------------------
// $response = MC::get_stores();
// $response = MC::add_store();



// Products Available
// as of Aug 10, 2022
// -------------------------------
  //    ID = Title (variant_id)
  //    -------------------------------
  //    GC = Gift Certification (gc1)
  //    TRIP = Rafting Trip Reservation (rt1)
  //    TEST001 = this is just a test (b01)



// Test add a Mailchimp Order.
// -------------------------------
// $mc_id = '358085136'; // Austin Openform Test Account
// $mc_store_id = 'arctic-ao'; // already setup in Mailchimp
// $response = MC::get_products();
// $response = MC::add_product();
// $response = MC::add_order();
// $response = MC::get_orders();


// $response = MC::get_member_activityfeed($list_id, 'sam@pcconsults.com', null, null, '1000', '0', ['note','order','ecommerce_signup','event']);
// $response = MC::get_member_activityfeed($list_id, 'sam@pcconsults.com', null, null, '1000', '0');


// Import the Subscribers first
// $import_subs = array();
// foreach($recentSales as $sub){
//   if(!$sub['subscribe']) continue;

//   $object = (object) array(
//     "email_address" => $sub['email'],
//     "status" => "subscribed",
//     "merge_fields" => array(
//       "FNAME" => $sub['namefirst'],
//       "LNAME" => $sub['namelast'],
//     ),
//   );
//   array_push($import_subs, $object);
// }
// $new_subs = MC::add_users_bulk_batchready($import_subs);

// // That returns an array of response info
// // What we need 
// $response = $new_subs;



// Try sending recent orders to MailChimp
// $response = MC::bulk_add_orders($recentSales);



echo '<h1>ArcticMC::run();</h1>';
echo '<pre>';
print_r($response);
echo '</pre>';
?>

