<?php 
/**
 * get inquiries and reservations from Arctic
 * send to Mailchimp
 */

namespace App\AO;  

require_once('lib/arctic.php');
require_once('lib/mailchimp.php');
use \App\Helpers\Arctic\Arctic as ArcticHelper;
use App\Helpers\Mailchimp\Mailchimp as MC;


/*
1. get all inquiries
  add to new people array
    index on the email address.

2. get all sales
  add/update new people array
    index on the email address.

3. Batch and send to Mailcimp


to run cron

use App\AO\Subs as Subs;
Subs::run();

*/


class Subs
{ 
  /**    
    * Prints a message to the debug file that can easily be called by any subclass.     
    *     
    * @param mixed $message      an object, array, string, number, or other data to write to the debug log    
    * @param bool  $shouldNotDie whether or not the The function should exit after writing to the log     
    *     
    */   
  protected static function log($message, $shouldNotDie = false) {
    error_log(print_r($message, true));   
    if ($shouldNotDie) {
      exit;
    }
  }
  
  public static function run() {

  
    $time1 = time();
    self::log('Initiating Subs Cron process run()');
        
    // $recent_inquiries_people = ArcticHelper::recentInquiryPeople(2);
    // $time2 = time();
    // self::log('Got inquiries');
    
    $recent_reservations_people = ArcticHelper::recentReservationsPeople(2);
    $time3 = time();
    self::log('Got reservations');


    //
    // #1
    // BUILD single array 
    // indexed by email
    // Purpose: remove duplicates by having sales override inquiries
    $subscribers = array();


    // GET Inquiries
    // foreach ($recent_inquiries_people as $inquiry) {
    //   // array (size=6)
    //   //     'created_date' => string '2020-10-22 09:59:56' (length=19)
    //   //     'namefirst' => string 'Jennifer' (length=8)
    //   //     'namelast' => string 'Cheshire' (length=8)
    //   //     'interest' => string 'full oar' (length=8)
    //   //     'tripid' => int 1213
    //   //     'email' => string 'jkc531@yahoo.com' (length=16)
    //   //     'subscribe' => boolean 
    //   if($inquiry['subscribe']){
    //     $this_email = $inquiry['email'];
    //     $subscribers[$this_email] = $inquiry;
    //   }
    // }
    // self::log('Inquiries to import: ' . count($subscribers));
    // $time5 = time();


    // GET Reservations
    foreach ($recent_reservations_people as $res) {
      // array (size=9)
      //     'tripid' => int 1208
      //     'activityid' => int 32429
      //     'guests' => int 2
      //     'personid' => int 49836
      //     'activity_start' => string '2021-04-03 00:00:00' (length=19)
      //     'activity_createdon' => string '2020-10-23 12:43:11' (length=19)
      //     'namefirst' => string 'Jeff' (length=4)
      //     'namelast' => string 'HOLD - AGC' (length=10)
      //     'email' => string 'info@advantagegrandcanyon.com' (length=29)
      if($res['subscribe']){
        $this_email = $res['email'];
        $subscribers[$this_email] = $res;
      }
    }
    self::log('Total to import: ' . count($subscribers));
    $time6 = time();
    self::log('Built Subscribers array');



    //
    // #2
    // PREP for Email List Import
    // Reformat Subscribers in Import style
    //
    $import_subs = array();
    foreach($subscribers as $sub){
      $object = (object) array(
        "email_address" => $sub['email'],
        "status" => "subscribed",
        "merge_fields" => array(
          "FNAME" => $sub['namefirst'],
          "LNAME" => $sub['namelast'],
        ),
      );
      array_push($import_subs, $object);
      self::log('Processed: new subscriber ' . $sub['email']);
    }
    $time7 = time();



    // 
    // #3
    // SEND TO MAILCHIMP
    // loop through each and send. 
    $new_subs = MC::add_users_bulk_batchready($import_subs);
    $time8 = time();
    self::log('Sent to Mailchimp: ' . print_r(count($subscribers), true));
    self::log('Created in Mailchimp: ' . print_r($new_subs->total_created, true));
    self::log('Updated in Mailchimp: ' . print_r($new_subs->total_updated, true));
    self::log('Error not imported in Mailchimp: ' . print_r($new_subs->error_count, true));

    //
    // #4
    // SEnd notification to email to me
    // $to = 'austin@openform.co';
    // $subject = 'ARR Cron - Add Subscribers Result';
    // $body = 'NOTICES:  ' . $new_subs;
    // $headers = array('Content-Type: text/html; charset=UTF-8');     
    // wp_mail( $to, $subject, $body, $headers );

    // $time9 = time();
    // self::log('Emailed results');

    $times = array(
      'one' => $time3 - $time1,
      'two' => $time6 - $time3,
      'six' => $time7 - $time6,
      'seven' => $time8 - $time7
    );

    // print the time it took for each step in SECONDS
    self::log('Timing: reservations: ' . $times['one'] .  
      ' — check reservation: ' . $times['two'] . 
      ' — prep subs array: ' . $times['six'] . 
      ' — import MC: ' . $times['seven']);

    return 'complete';
  }
}

?>