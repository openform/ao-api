<?php

// Trigger Arctic to Mailchimp service
require_once('lib/arctic.php');
require_once('lib/mailchimp.php');
require_once('lib/mailgun.php');
require_once('crons.php');
use \App\Helpers\Arctic\Arctic as ArcticHelper;
use App\Helpers\Mailchimp\Mailchimp as MC;
use App\Helpers\Mailgun\Email as Email;
use App\AO\Subs as Subs;

// $inquiries = ArcticHelper::recentInquiryPeople();
// $reservations = ArcticHelper::recentReservationsPeople();

// $mc = MC::add_user('test@openform.co', 'test add');
// $mc = MC::add_users_bulk(array(
//   ['email'=> 'one@openform.co','fname'=>'billy','lname'=>'bobby'],
//   ['email'=> 'two@openform.co','fname'=>'jon','lname'=>'smitherin'],
//   ['email'=> 'three@openform.co','fname'=>'suzie','lname'=>'queue ball']
// ));
// $mc = MC::get_lists();
// if($mc){ echo "Mailchimp Connect :: "; print_r($mc); }

Subs::run();
?>

